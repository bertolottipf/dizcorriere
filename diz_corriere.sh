#!/bin/bash

########################################################################################
# Questo script necessita di curl, xhtml2pdf e pup (https://github.com/ericchiang/pup) #
########################################################################################

pup_dir="~/go/bin/"

#dir_name='diz_corriere'
#mkdir $(echo $dir_name)
#cd $(echo $dir_name)

url_base="http://dizionari.corriere.it/dizionario_italiano/"
pages_ext=".shtml"

lettere=("a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m" "n" "o" "p" "q" "r" "s" "t" "u" "v" "w" "x" "y" "z")
url=""

contenuto='<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Dizionario Corriere - il Sabatini Coletti</title>
	<link rel="stylesheet" href="http://css2.corriereobjects.it/includes2013/LIBS/css/whitelabel_2015.css?v=20170713092938">
	<style type="text/css" media="screen, projection">
		<!--
		@import url("http://css.corriere.it/includes2007/static/css/dizionari-2011.css");
		-->
	</style>
	<style type="text/css">
		#defin-dx {
			width: 100%
		}
	</style>
</head>
<body>
	<h1>Dizionario Corriere - il Sabatini Coletti</h1>
	<!-- <img src="https://images-na.ssl-images-amazon.com/images/I/817VGQm-WOL.jpg" alt="il Sabatini Coletti - Dizionario della Lingua Italiana"> -->'


cambia_lettera=true
numero_lettera=1
page=1
file_result="diz.html"
file_ultima_pagina="ultima_pagina.num"

# vedo se c'è gia una pagina esaminata
if [ -f "$file_ultima_pagina" ]; then
	ultima_pagina="`cat $file_ultima_pagina`"
	#echo $ultima_pagina

	# prendo la lettera
	lettera=${ultima_pagina:49:1}
	# numero_lettera posizione lettera
	for i in "${!lettere[@]}"; do
		if [[ "${lettere[$i]}" = "${lettera}" ]]; then
			numero_lettera=$[$i+1]
		fi
	done

	# prendo il numero di pagina
	page=${ultima_pagina:51}
	IFS='.' read -ra ADDR <<< "$page"
	for i in "${ADDR[@]}"; do
		if [[ $i -eq 0 ]]; then
			page=${ADDR[0]}
		fi
	done

	is_number='^[0-9]+$'
	#echo $lettera
	#echo $page

	# se la pagina non è un numero la pagina è 2 perché comunque esiste il file sebbene non contenga
	# un numero di pagina esplicito che quindi è già 1 cha +1 fà quindi 2
	# altrimenti aumento la pagina di uno
	if ! [[ $page =~ $is_number ]]; then
		page=2
	else
		page=$[$page+1]
	fi
else
	# se non c'è un file aggiungo l'header
	echo $contenuto >> $file_result
fi

echo "numero_lettera "$numero_lettera
echo "lettera "$lettera
echo "page "$page

while $cambia_lettera; do 
	# cerco una pagina della lettera 
	# finché il titolo della pagina non è "Pagina non disponibile"
	cicla_lettera=true
	#page=1
	url="${url_base}${lettere[$numero_lettera-1]}$pages_ext"
	while $cicla_lettera; do

		# se il contatore $page è maggiore di 1 aggiungo _$page dopo la lettera
		if [ $page -gt 1 ]; then
			url="${url_base}${lettere[$numero_lettera-1]}_$page$pages_ext"
		fi
		#echo $url

		# prendo il titolo della pagina
		titolo=`curl -s $url| ~/go/bin/pup 'h1'`

		readarray -t titolo <<<"$titolo"
		#echo "titolo = $titolo"

		# se il titolo è diverso da quello della 404
		if [ "$titolo" == '' ]; then
			
			# prendo gli url dei lemmi
			links_string=`curl -s $url | ~/go/bin/pup '#ris-main > ul > li > a attr{href}'`
			#echo $links_string

			# splitto la stringa in array 
			readarray -t links <<<"$links_string"
			#echo $links

			# prendo il significato della parola da ogni url preso
			contenuto=""
			for link in "${links[@]}"
			do
				echo $link
				def=`curl -s ${url_base}$link | ~/go/bin/pup '#defin-dx'`
				#echo $def 
				contenuto=$contenuto$def
				#echo $contenuto
			done

			# salvo aggiungendo i significati delle parole trovate nella pagina
			# solo ora così da poter tenere poi traccia della pagina analizzata
			echo $contenuto >> $file_result

			# salvo la pagina analizzata
			touch $file_ultima_pagina
			echo $url > $file_ultima_pagina
		else
			echo "${lettere[$numero_lettera-1]}_$page"
			cicla_lettera=false
			page=0
			if [ "${lettere[$numero_lettera-1]}" == "z" ]; then
				cambia_lettera=false
			fi
		fi

		#page=$((page+1))
		page=$[$page+1]
	done
	numero_lettera=$[$numero_lettera+1]
done

echo "</body></html>" >> $file_result

xhtml2pdf $file_result